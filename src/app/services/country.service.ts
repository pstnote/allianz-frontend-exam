import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Country } from '../models/country';

@Injectable({
  providedIn: 'root',
})
export class CountryService {
  private url = 'https://restcountries.eu/rest/v2/all';
  private subject = new BehaviorSubject([]);
  private subject2 = new BehaviorSubject([]);
  countries = this.subject.asObservable();
  countriesAll = this.subject2.asObservable();

  constructor(private http: HttpClient) {}

  storeCountries(countries: Country[]): Observable<Country[]> {
    this.subject2.next(countries);
    return of(countries);
  }

  updateCountries(countries: Country[]): Observable<Country[]> {
    this.subject.next(countries);
    return of(countries);
  }

  /** GET all countries */
  getContries(): Observable<Country[]> {
    return this.http.get<Country[]>(this.url).pipe(
      catchError(this.handleError<Country[]>('getHeroes', []))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
