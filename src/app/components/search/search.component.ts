import { Component, OnInit } from '@angular/core';
import { Observable, Subscription, Subject, of } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from 'rxjs/operators';
import { Country } from '../../models/country';
import { CountryService } from '../../services/country.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  private searchTerms = new Subject<string>();
  countries$: Observable<Country[]>;
  countries: Country[] = [];
  subscription: Subscription;

  constructor(private coutryService: CountryService) {}

  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.subscription = this.coutryService.countriesAll.subscribe(
      (countries) => (this.countries = countries)
    );
    this.countries$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
      // ignore new term if same as previous term
      distinctUntilChanged(),
      // switch to new search observable each time the term changes
      switchMap((term: string) => {
        // filter by country name
        return of(
          this.countries.filter(
            (country) =>
              country.name &&
              country.name
                .toLocaleLowerCase()
                .indexOf(term.toLocaleLowerCase()) > -1
          )
        );
      })
    );
    this.countries$.subscribe({
      next: (countries) => {
        this.coutryService.updateCountries(countries);
      },
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
