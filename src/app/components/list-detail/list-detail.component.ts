import { Component, OnInit, Input } from '@angular/core';
import { Country } from '../../models/country';

@Component({
  selector: 'app-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.css']
})
export class ListDetailComponent implements OnInit {

  @Input() country?: Country;

  constructor() { }

  ngOnInit(): void {
  }

}
