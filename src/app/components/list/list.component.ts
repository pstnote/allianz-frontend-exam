import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Country } from '../../models/country';
import { CountryService } from '../../services/country.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  countries: Country[] = [];
  subscription: Subscription;

  constructor(private coutryService: CountryService) {}

  ngOnInit(): void {
    this.getContries();
    this.subscription = this.coutryService.countries.subscribe(countries => this.countries = countries);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getContries(): void {
    this.coutryService.getContries().subscribe((countries) => {
      this.coutryService.updateCountries(countries);
      this.coutryService.storeCountries(countries);
    });
  }
}
