# Allianz Frontend Exam

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.5.

## Installation
1. Clone Project
``
git clone git@gitlab.com:pstnote/allianz-frontend-exam.git
``
2. Install the Angular CLI
``
npm install -g @angular/cli
``
3. Go to project folder and install dependencies 
``
npm install
``


## Run Application
1. Run with Angular CLI
```
ng serve -o
```
2. Open browser and navigate to
```
http://localhost:4200
```


